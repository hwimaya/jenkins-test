#!groovy

def slack_channel = '#general'
def isFeatureBranch = !(BRANCH_NAME == 'develop' || BRANCH_NAME == 'master')
def branch        = BRANCH_NAME
def build_id      = env.build_id

def TERRAFORM_VERSION = "0.12.16"

pipeline {
  agent any

  post {
    always {
      script {
        build_status =  currentBuild.result
        echo "build_status = " + build_status

        subject = "${build_status}: Job '${env.JOB_NAME} [Build ${env.BUILD_NUMBER}]'"
        summary = "${subject} (${env.BUILD_URL})"

        if (build_status == 'SUCCESS') {
          colorCode = '#00FF00'       // GREEN
        } else {
          colorCode = '#FF0000'       // RED
        }
        slackSend (channel: slack_channel, color: colorCode, message: summary)
      }
    }
  }

  stages {

    stage('Env Variables') {
      steps {
        sh "printenv"
      }
    }

    stage('checkout') {
      steps {
        git 'https://hwimaya@bitbucket.org/hwimaya/jenkins-test.git'
      }
    }

    stage ("Feature or PR or Develop branch") {
      when { expression { BRANCH_NAME != 'master' }}
      stages {

        stage('web packer build') {
          steps {
            sh '''cd terraform
            ARTIFACT_WEB=`packer build -machine-readable packer-ami-web.json |awk -F, \'$0 ~/artifact,0,id/ {print $6}\'`
            AMI_ID_WEB=`echo $ARTIFACT_WEB | cut -d \':\' -f2`
            echo \'variable "WEB_INSTANCE_AMI" { default = "\'${AMI_ID_WEB}\'" }\' > amivar_web.tf
            aws s3 cp amivar_web.tf s3://build-landing-aws-jenkins-terraform-hwimaya/amivar_web.tf'''
          }
        }

        stage('deploy') {
          steps {
            sh '''set +x
            cd terraform
            aws s3 cp s3://build-landing-aws-jenkins-terraform-hwimaya/amivar_web.tf amivar_web.tf
            terraform init
            terraform apply -auto-approve'''
          }
        }

        stage("Approval Step") {
          steps {
            input id: "ApprovalStep" , message: "Do you want to destroy Develop system?"
          }
        }

        stage('destroy') {
          steps {
            sh '''set +x
            cd terraform
            terraform destroy -auto-approve'''
          }
        }

      }
    }

    stage("Master branch") {
      when { expression { BRANCH_NAME == 'master' }}
      stages {
        stage('deploy') {
          steps {
            sh '''set +x
            cd terraform
            aws s3 cp s3://build-landing-aws-jenkins-terraform-hwimaya/amivar_web.tf amivar_web.tf
            terraform init
            terraform apply -auto-approve'''
          }
        }

        stage("Approval Step") {
          steps {
            input id: "ApprovalStep" , message: "Do you want to destroy Production system?"
          }
        }

        stage('destroy') {
          steps {
            sh '''set +x
            cd terraform
            terraform destroy -auto-approve'''
          }
        }
      }
    }
  }
}