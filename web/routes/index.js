var express = require('express');
var router = express.Router();
var request = require('request');


var api_url = process.env.API_HOST + '/api/status';

/* GET home page. */
router.get('/', function(req, res) {
        res.render('index');
});

module.exports = router;
